﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace system_prototype
{
    class Test
    {
        private Char type;
        private int testID;
        private Boolean completed = false;
        private Boolean passed = false;

        public Test (Char type, int IDno)
        {
            this.type = type;
            this.testID = IDno;
        }

        public Boolean Completed
        {
            get { return completed; }            
        }

        public Boolean Passed
        {
            get { return passed; }
        }

        public Char typeRet
        {
            get { return type; }
        }

        public int IDRet
        {
            get { return testID; }
        }

        public Boolean changeComplete
        {
            set { completed = value; }
        }

        public Boolean passComplete
        {
            set { this.passed = value; }
        }

        //public void testMenu()
        //{
        //    int testSwitch;
        //    String check;
        //    int pass;

        //    do
        //    {
        //        Console.WriteLine("1. Mark test as complete");
        //        Console.WriteLine("0. Exit to Project menu");
        //        testSwitch = Convert.ToInt32(Console.ReadLine());
        //        switch (testSwitch)
        //        {
        //            case 1:
        //                Console.WriteLine("Are you sure you want to mark this test as complete? y/n");
        //                check = Console.ReadLine();
        //                if (check == "y" || check == "yes")
        //                {
        //                    Console.WriteLine("Has the spool passed the test?");
        //                    Console.WriteLine("1. Yes");
        //                    Console.WriteLine("2. No");
        //                    pass = Convert.ToInt32(Console.ReadLine());
        //                    do
        //                    {
        //                        Console.WriteLine("Input error, please try again");
        //                        Console.WriteLine("Has the spool passed the test?");
        //                        Console.WriteLine("1. Yes");
        //                        Console.WriteLine("2. No");
        //                        pass = Convert.ToInt32(Console.ReadLine());

        //                    } while (pass != 1 || pass != 2);
        //                    if (pass == 1)
        //                    {
        //                        passed = true;
        //                        Console.WriteLine("Test record updated");
        //                    }
        //                    if (pass == 2)
        //                    {
        //                        passed = false;
        //                        Console.WriteLine("Test record updated");
        //                    }
        //                }
        //                else
        //                    Console.WriteLine("Returning to menu");
        //                Console.WriteLine();
        //                break;
        //            default:
        //                break;
        //        }
        //    } while (testSwitch != 0);
        //}
    }
}
