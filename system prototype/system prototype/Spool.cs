﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace system_prototype
{
    class Spool
    {
        private int ID_no;
        private double cost;
        private double man_hours;
        private List<Stage> listofstages = new List<Stage>();       

        public Spool (int ID_no)
        {
            this.ID_no = ID_no;
        }

        public double Cost
        {
            get { return cost; }
        }

        public double ManHours
        {
            get { return man_hours; }
        }

        public int spoolID
        {
            get { return ID_no; }
        }

        public List<Stage> getStageList
        {
            get { return listofstages; }
        }

        public double newCost
        {
            set { cost = value; }
        }

        public double newHoursWorked
        {
            set { man_hours = value; }
        }

        public void addToStageList(String name, double price, double noHours)
        {
            listofstages.Add(new Stage(name, price, noHours));
        }

        //public void spoolMenu()
        //{
        //    int spoolSwitch;
        //    String findStage;
        //    String tempName;
        //    double tempCost;
        //    double tempHours;

        //    do
        //    {
        //        Console.WriteLine("Please Chose a menu option: ");
        //        Console.WriteLine("1. Add a stage to the current spool");
        //        Console.WriteLine("2. Display list of stages and their completion status for current spool");
        //        Console.WriteLine("3. Display current cost for current spool");
        //        Console.WriteLine("4. Display current man hours worked on current spool");
        //        Console.WriteLine("5. Enter stage menu");
        //        Console.WriteLine("0. Exit to Project menu");
        //        spoolSwitch = Convert.ToInt32(Console.ReadLine());

        //        switch (spoolSwitch)
        //        {
        //            case 1:
        //                Console.WriteLine("Please enter a name for the current stage");
        //                tempName = Console.ReadLine();
        //                Console.WriteLine("Please enter the cost of this stage");
        //                tempCost = Convert.ToDouble(Console.ReadLine());
        //                Console.WriteLine("Please enter the number of hours work for this stage");
        //                tempHours = Convert.ToDouble(Console.ReadLine());
        //                listofstages.Add(new Stage(tempName, tempCost, tempHours));
        //                cost = cost + tempCost;
        //                man_hours = man_hours + tempHours;
        //                tempName = null;
        //                tempCost = 0;
        //                tempHours = 0;
        //                Console.WriteLine();
        //                break;
        //            case 2:
        //                int z = 1;
        //                foreach (Stage stage in listofstages)
        //                {
        //                    if (stage.checkComplete)
        //                        Console.WriteLine(z + stage.Stagename + ": complete");
        //                    else
        //                        Console.WriteLine(z + stage.Stagename + ": in progress");
        //                    z++;
        //                }
        //                Console.WriteLine();
        //                break;
        //            case 3:
        //                Console.WriteLine("Current cost of spool: £" + cost);
        //                Console.WriteLine();
        //                break;
        //            case 4:
        //                Console.WriteLine("Current number of hours worked on spool: " + man_hours + " hours");
        //                Console.WriteLine();
        //                break;
        //            case 5:
        //                Console.WriteLine("Please enter the name of the Stage you wish to view: ");
        //                findStage = Console.ReadLine();
        //                Stage temp = listofstages.Find(x => x.Stagename.Contains(findStage));
        //                temp.stageMenu();
        //                findStage = null;
        //                Console.WriteLine();
        //                break;
        //            default:
        //                break;
        //        }
        //    } while (spoolSwitch != 0);
        //}
    }
}
