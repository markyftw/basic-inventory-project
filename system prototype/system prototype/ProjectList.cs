﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace system_prototype
{
    class ProjectList
    {
        List<Project> listofprojects = new List<Project>();

        public ProjectList (){}

        public List<Project> getProjList
        {
            get { return listofprojects; }
        }

        public void addToProjList(String ID)
        {
            listofprojects.Add(new Project(ID));
        }
    }
}
