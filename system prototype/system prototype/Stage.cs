﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace system_prototype
{
    class Stage
    {
        private String stage_name;
        private double cost_of_stage;
        private double hours_of_stage_labour;
        private Boolean isComplete;
        private List<Weld> listofwelds = new List<Weld>();
        private List<Test> listoftests = new List<Test>();

        public Stage (String stagename, double cost, double hours)
        {
            this.stage_name = stagename;
            this.cost_of_stage = cost;
            this.hours_of_stage_labour = hours;
            isComplete = false;
        }

        public String Stagename
        {
            get { return stage_name; }
        }

        public double Cost
        {
            get { return cost_of_stage; }
        }

        public double HoursWorked
        {
            get { return hours_of_stage_labour; }
        }

        public Boolean checkComplete
        {
            get { return isComplete; }
        }

        public List<Weld> getWeldList
        {
            get { return listofwelds; }
        }

        public List<Test> getTestList
        {
            get { return listoftests; }
        }

        public Boolean setToComplete
        {
            set { isComplete = value; }
        }

        public void addToWeldList(Char wType, int wID)
        {
            listofwelds.Add(new Weld(wType, wID));
        }

        public void addToTestList(Char tType, int tID)
        {
            listoftests.Add(new Test(tType, tID));
        }

        //public void stageMenu()
        //{
        //    int stageSwitch;
        //    Char tempWeld;
        //    Char tempTest;
        //    int tempWeldID;
        //    int tempTestID;
        //    String check;
        //    Boolean tempComp;
        //    Boolean tempPass;


        //    do
        //    {
        //        Console.WriteLine("Please Chose a menu option: ");
        //        Console.WriteLine("1. Add a weld to the current stage");
        //        Console.WriteLine("2. Add a test to the current stage");
        //        Console.WriteLine("3. Mark Stage as complete");
        //        Console.WriteLine("4. Display stage weld and test information");
        //        Console.WriteLine("5. Enter test menu");
        //        Console.WriteLine("0. Exit to Project menu");
        //        stageSwitch = Convert.ToInt32(Console.ReadLine());

        //        switch (stageSwitch)
        //        {
        //            case 1:
        //                Console.WriteLine("Please enter the Weld type to be carried out");
        //                tempWeld = Convert.ToChar(Console.ReadLine());
        //                Console.WriteLine("Please enter the welder's ID no.");
        //                tempWeldID = Convert.ToInt32(Console.ReadLine());
        //                listofwelds.Add(new Weld(tempWeld, tempWeld));
        //                Console.WriteLine();
        //                break;
        //            case 2:
        //                Console.WriteLine("Please enter the type of Test to be carried out");
        //                tempTest = Convert.ToChar(Console.ReadLine());
        //                Console.WriteLine("Please enter an ID no for the Test");
        //                tempTestID = Convert.ToInt32(Console.ReadLine());
        //                listoftests.Add(new Test(tempTest, tempTestID));
        //                Console.WriteLine();
        //                break;
        //            case 3:
        //                Console.WriteLine("Are you sure you want to mark this stage as complete? y/n");
        //                check = Console.ReadLine();
        //                if (check == "y" || check == "yes")
        //                {
        //                    isComplete = true;
        //                    Console.WriteLine("Stage has been completed");
        //                }
        //                else
        //                {
        //                    Console.WriteLine("Stage has been left incomplete");
        //                    Console.WriteLine("Returning to menu");
        //                }
        //                Console.WriteLine();
        //                break;
        //            case 4:
        //                Console.WriteLine("Welds carried out in this stage: ");
        //                foreach (Weld weld in listofwelds)
        //                {
        //                    Console.WriteLine("Type: " + weld.Weld_type + " carried out by welder no. " + weld.WelderID);
        //                }
        //                Console.WriteLine();
        //                Console.WriteLine("Tests carried out in this stage: ");
        //                foreach (Test test in listoftests)
        //                {
        //                    Console.WriteLine("ID no. " + test.IDRet + "Type: " + test.typeRet);
        //                    Console.WriteLine("Status: ");
        //                    tempComp = test.Completed;
        //                    tempPass = test.Passed;
        //                    if (tempComp)
        //                    {
        //                        Console.WriteLine("Completed");
        //                        if (tempPass)
        //                            Console.WriteLine("Passed");
        //                        else
        //                            Console.WriteLine("Failed");
        //                    }
        //                    else
        //                        Console.WriteLine("Not yet carried out");
                            
        //                }
        //                Console.WriteLine();
        //                break;
        //            case 5:
        //                Console.WriteLine("Please enter the ID nuber of the test you wish to edit");
        //                tempTestID = Convert.ToInt32(Console.ReadLine());
        //                Test temp = listoftests.Find(x => x.IDRet.Equals(tempTestID));
        //                temp.testMenu();
        //                temp = null;
        //                Console.WriteLine();
        //                break;
        //            default:
        //                break;
        //        }
        //    } while (stageSwitch != 0);
        //}
    }
}
