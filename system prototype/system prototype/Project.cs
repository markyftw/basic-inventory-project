﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace system_prototype
{
    class Project
    {
        private List<Spool> listofspools = new List<Spool>();
        private double total_cost = 0;
        private double total_man_hours = 0;
        private String projectName;

        public Project (String projName)
        {
            projectName = projName;
        }

        public String proNam
        {
            get { return projectName; }
        }

        public List<Spool> getSpoolList
        {
            get { return listofspools; }
        }

        public double getCost
        {
            get { return total_cost; }
        }

        public double getHours
        {
            get { return total_man_hours; }
        }

        public double setCost
        {
            set { total_cost = value; }
        }

        public double setHours
        {
            set { total_man_hours = value; }
        }

        public void addToSpoolList(int ID)
        {
            listofspools.Add(new Spool(ID));
        }

        //public void projectMenu()
        //{
        //    int projMenuSwitch;
        //    int tempID;
            
        //    do
        //    {
        //        Console.WriteLine("Please choose a menu option: ");
        //        Console.WriteLine("1. Add a new Spool to the Project");
        //        Console.WriteLine("2. Display list of spools in current project");
        //        Console.WriteLine("3. Display total cost of project");
        //        Console.WriteLine("4. Display total man hours on project");
        //        Console.WriteLine("5. Enter spool menu");
        //        Console.WriteLine("0. Exit to main menu");
        //        projMenuSwitch = Convert.ToInt32(Console.ReadLine());
        //        switch (projMenuSwitch)
        //        {
        //            case 1:
        //                // add a new spool
        //                Console.WriteLine("Please enter an ID no. for spool: ");
        //                tempID = Convert.ToInt32(Console.ReadLine());
        //                listofspools.Add(new Spool(tempID));
        //                tempID = 0;
        //                Console.WriteLine();
        //                break;
        //            case 2:
        //                // display list of spools in current project
        //                Console.WriteLine("This project currently contains spools noumbered: ");
        //                foreach (Spool spool in listofspools)
        //                {
        //                    Console.WriteLine(spool.spoolID);
        //                }
        //                Console.WriteLine();
        //                break;
        //            case 3:
        //                Console.WriteLine("The project's overall total cost is: ");
        //                foreach (Spool spool in listofspools)
        //                {
        //                    total_cost += spool.Cost;
        //                }
        //                Console.WriteLine("£" + total_cost);
        //                total_cost = 0;
        //                Console.WriteLine();
        //                break;
        //            case 4:
        //                Console.WriteLine("The total amount of man hours worked on the project: ");
        //                foreach (Spool spool in listofspools)
        //                {
        //                    total_man_hours += spool.ManHours;
        //                }
        //                Console.WriteLine(total_man_hours + " hours");
        //                total_man_hours = 0;
        //                Console.WriteLine();
        //                break;
        //            case 5:
        //                Console.WriteLine("Please enter the Spool ID number: ");
        //                tempID = Convert.ToInt32(Console.ReadLine());
        //                Spool temp = listofspools.Find(x => x.spoolID.Equals(tempID));
        //                temp.spoolMenu();
        //                Console.WriteLine();
        //                break;
        //            default:
        //                break;
        //        }
        //    } while (projMenuSwitch != 0);
        //}
    }
}
