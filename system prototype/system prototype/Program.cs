﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace system_prototype
{
    class Program
    {        
        static void topMenu(ProjectList tempList)
        {
            String projName;
            String findProj;
            int mainMenu;            

            do
            {
                Console.WriteLine("===========================================");
                Console.WriteLine("Please choose a menu option: ");
                Console.WriteLine("1. Add a new Project");
                Console.WriteLine("2. Display List of Projects and their Names");
                Console.WriteLine("3. Enter the Project menu");
                Console.WriteLine("0. Exit");
                Console.WriteLine("===========================================");
                Console.WriteLine();
                mainMenu = Convert.ToInt32(Console.ReadLine());
                switch (mainMenu)
                {
                    case 1:
                        //add a new project
                        Console.WriteLine("===========================================");
                        Console.WriteLine("Please enter a name for the project: ");
                        Console.WriteLine("===========================================");
                        Console.WriteLine();
                        projName = Console.ReadLine();
                        tempList.addToProjList(projName);
                        Console.WriteLine();
                        break;
                    case 2:
                        // display list of projects and their names
                        int z = 1;
                        foreach (Project proj in tempList.getProjList)
                        {
                            Console.WriteLine(z + ". " + proj.proNam);
                            z++;
                        }
                        Console.WriteLine();
                        break;
                    case 3:
                        //project menu
                        Console.WriteLine("===========================================");
                        Console.WriteLine("Please enter the name of the Project you wish to view: ");
                        Console.WriteLine("===========================================");
                        Console.WriteLine();
                        findProj = Console.ReadLine();
                        Project tempProj = tempList.getProjList.Find(x => x.proNam.Contains(findProj));
                        if (tempProj != null)
                            projectMenu(tempList, tempProj);
                        else
                        {
                            Console.WriteLine("===========================================");
                            Console.WriteLine("This project does not yet exist");
                            Console.WriteLine("Please create the project before accessing the menu");
                            Console.WriteLine("===========================================");
                        }
                        findProj = null;
                        Console.WriteLine();
                        break;
                    default:
                        break;
                }
            } while (mainMenu != 0);
        }

        static void projectMenu(ProjectList tempList, Project tempProj)
        {
            int projMenuSwitch;
            int tempID;

            do
            {
                Console.WriteLine("===========================================");
                Console.WriteLine("Please choose a menu option: ");
                Console.WriteLine("1. Add a new Spool to the Project");
                Console.WriteLine("2. Display list of spools in current project");
                Console.WriteLine("3. Display total cost of project");
                Console.WriteLine("4. Display total man hours on project");
                Console.WriteLine("5. Enter spool menu");
                Console.WriteLine("0. Exit to main menu");
                Console.WriteLine("===========================================");
                Console.WriteLine();
                projMenuSwitch = Convert.ToInt32(Console.ReadLine());
                switch (projMenuSwitch)
                {
                    case 1:
                        // add a new spool
                        Console.WriteLine("===========================================");
                        Console.WriteLine("Please enter an ID no. for spool: ");
                        Console.WriteLine("===========================================");
                        Console.WriteLine();
                        tempID = Convert.ToInt32(Console.ReadLine());
                        tempProj.addToSpoolList(tempID);
                        tempID = 0;
                        Console.WriteLine();
                        break;
                    case 2:
                        // display list of spools in current project
                        Console.WriteLine("===========================================");
                        Console.WriteLine("This project currently contains spools noumbered: ");
                        foreach (Spool spool in tempProj.getSpoolList)
                        {
                            Console.WriteLine(spool.spoolID);
                        }
                        Console.WriteLine("===========================================");
                        Console.WriteLine();
                        break;
                    case 3:
                        Console.WriteLine("===========================================");
                        Console.WriteLine("The project's overall total cost is: ");
                        foreach (Spool spool in tempProj.getSpoolList)
                        {
                            tempProj.setCost = tempProj.getCost + spool.Cost;
                        }
                        Console.WriteLine("£" + tempProj.getCost);
                        Console.WriteLine("===========================================");
                        Console.WriteLine();
                        break;
                    case 4:
                        Console.WriteLine("===========================================");
                        Console.WriteLine("The total amount of man hours worked on the project: ");
                        foreach (Spool spool in tempProj.getSpoolList)
                        {
                            tempProj.setHours = tempProj.getHours + spool.ManHours;
                        }
                        Console.WriteLine(tempProj.getHours + " hours");
                        Console.WriteLine("===========================================");
                        Console.WriteLine();
                        break;
                    case 5:
                        Console.WriteLine("===========================================");
                        Console.WriteLine("Please enter the Spool ID number: ");
                        Console.WriteLine("===========================================");
                        Console.WriteLine();
                        tempID = Convert.ToInt32(Console.ReadLine());
                        Spool tempSpool = tempProj.getSpoolList.Find(x => x.spoolID.Equals(tempID));
                        if (tempSpool != null)
                            spoolMenu(tempList, tempProj, tempSpool);
                        else
                        {
                            Console.WriteLine("===========================================");
                            Console.WriteLine("This spool does not yet exist");
                            Console.WriteLine("Please create the spool before accessing the menu");
                            Console.WriteLine("===========================================");
                        }
                        Console.WriteLine();
                        break;
                    default:
                        break;
                }
            } while (projMenuSwitch != 0);
            topMenu(tempList);
        }

        static void spoolMenu(ProjectList tempList, Project tempProj, Spool tempSpool)
        {
            int spoolSwitch;
            String findStage;
            String tempName;
            double tempCost;
            double tempHours;

            do
            {
                Console.WriteLine("===========================================");
                Console.WriteLine("Please Chose a menu option: ");
                Console.WriteLine("1. Add a stage to the current spool");
                Console.WriteLine("2. Display list of stages and their completion status for current spool");
                Console.WriteLine("3. Display current cost for current spool");
                Console.WriteLine("4. Display current man hours worked on current spool");
                Console.WriteLine("5. Enter stage menu");
                Console.WriteLine("0. Exit to Project menu");
                Console.WriteLine("===========================================");
                Console.WriteLine();
                spoolSwitch = Convert.ToInt32(Console.ReadLine());

                switch (spoolSwitch)
                {
                    case 1:
                        Console.WriteLine("===========================================");
                        Console.WriteLine("Please enter a name for the current stage");
                        Console.WriteLine();
                        tempName = Console.ReadLine();
                        Console.WriteLine();
                        Console.WriteLine("Please enter the cost of this stage");
                        Console.WriteLine();
                        tempCost = Convert.ToDouble(Console.ReadLine());
                        Console.WriteLine();
                        Console.WriteLine("Please enter the number of hours work for this stage");
                        Console.WriteLine();
                        tempHours = Convert.ToDouble(Console.ReadLine());
                        Console.WriteLine();
                        tempSpool.addToStageList(tempName, tempCost, tempHours);
                        tempSpool.newCost = tempSpool.Cost + tempCost;
                        tempSpool.newHoursWorked = tempSpool.ManHours + tempHours;
                        tempName = null;
                        tempCost = 0;
                        tempHours = 0;
                        Console.WriteLine("===========================================");
                        Console.WriteLine();
                        break;
                    case 2:
                        int z = 1;
                        Console.WriteLine("===========================================");
                        foreach (Stage stage in tempSpool.getStageList)
                        {
                            if (stage.checkComplete)
                            {
                                Console.WriteLine(z + stage.Stagename + ": complete");
                                Console.WriteLine();
                            }
                            else
                            {
                                Console.WriteLine(z + stage.Stagename + ": in progress");
                                Console.WriteLine();
                            }
                            z++;
                        }
                        Console.WriteLine("===========================================");
                        Console.WriteLine();
                        break;
                    case 3:
                        Console.WriteLine("===========================================");
                        Console.WriteLine("Current cost of spool: £" + tempSpool.Cost);
                        Console.WriteLine("===========================================");
                        Console.WriteLine();
                        break;
                    case 4:
                        Console.WriteLine("===========================================");
                        Console.WriteLine("Current number of hours worked on spool: " + tempSpool.ManHours + " hours");
                        Console.WriteLine("===========================================");
                        Console.WriteLine();
                        break;
                    case 5:
                        Console.WriteLine("===========================================");
                        Console.WriteLine("Please enter the name of the Stage you wish to view: ");
                        Console.WriteLine("===========================================");
                        Console.WriteLine();
                        findStage = Console.ReadLine();
                        Stage tempStage = tempSpool.getStageList.Find(x => x.Stagename.Contains(findStage));
                        if (tempStage != null)
                            stageMenu(tempList, tempProj, tempSpool, tempStage);
                        else
                        {
                            Console.WriteLine("===========================================");
                            Console.WriteLine("This stage does not yet exist");
                            Console.WriteLine("Please create the stage before accessing the menu");
                            Console.WriteLine("===========================================");
                        }
                        findStage = null;
                        Console.WriteLine();
                        break;
                    default:
                        break;
                }
            } while (spoolSwitch != 0);
            projectMenu(tempList, tempProj);
        }

        static void stageMenu(ProjectList tempList, Project tempProj, Spool tempSpool, Stage tempStage)
        {
            int stageSwitch;
            Char tempWeld;
            Char tempTestType;
            int tempWeldID;
            int tempTestID;
            String check;


            do
            {
                Console.WriteLine("===========================================");
                Console.WriteLine("Please Chose a menu option: ");
                Console.WriteLine("1. Add a weld to the current stage");
                Console.WriteLine("2. Add a test to the current stage");
                Console.WriteLine("3. Mark Stage as complete");
                Console.WriteLine("4. Display stage weld and test information");
                Console.WriteLine("5. Enter test menu");
                Console.WriteLine("0. Exit to Project menu");
                Console.WriteLine("===========================================");
                Console.WriteLine();
                stageSwitch = Convert.ToInt32(Console.ReadLine());

                switch (stageSwitch)
                {
                    case 1:
                        Console.WriteLine("===========================================");
                        Console.WriteLine("Please enter the Weld type to be carried out");
                        Console.WriteLine();
                        tempWeld = Convert.ToChar(Console.ReadLine());
                        Console.WriteLine("Please enter the welder's ID no.");
                        Console.WriteLine();
                        tempWeldID = Convert.ToInt32(Console.ReadLine());
                        tempStage.addToWeldList(tempWeld, tempWeldID);
                        Console.WriteLine("===========================================");
                        Console.WriteLine();
                        break;
                    case 2:
                        Console.WriteLine("===========================================");
                        Console.WriteLine("Please enter the type of Test to be carried out");
                        Console.WriteLine();
                        tempTestType = Convert.ToChar(Console.ReadLine());
                        Console.WriteLine("Please enter an ID no for the Test");
                        Console.WriteLine();
                        tempTestID = Convert.ToInt32(Console.ReadLine());
                        tempStage.addToTestList(tempTestType, tempTestID);
                        Console.WriteLine("===========================================");
                        Console.WriteLine();
                        break;
                    case 3:
                        Console.WriteLine("===========================================");
                        Console.WriteLine("Are you sure you want to mark this stage as complete? y/n");
                        Console.WriteLine();
                        check = Console.ReadLine();
                        if (check == "y" || check == "yes")
                        {
                            tempStage.setToComplete = true;
                            Console.WriteLine("Stage has been completed");
                        }
                        else
                        {
                            Console.WriteLine("Stage has been left incomplete");
                            Console.WriteLine("Returning to menu");
                        }
                        Console.WriteLine("===========================================");
                        Console.WriteLine();
                        break;
                    case 4:
                        Console.WriteLine("===========================================");
                        Console.WriteLine("Welds carried out in this stage: ");
                        foreach (Weld weld in tempStage.getWeldList)
                        {
                            Console.WriteLine("Type: " + weld.Weld_type + " carried out by welder no. " + weld.WelderID);
                            Console.WriteLine();
                        }
                        Console.WriteLine("===========================================");
                        Console.WriteLine();
                        Console.WriteLine("===========================================");
                        Console.WriteLine("Tests carried out in this stage: ");
                        foreach (Test test in tempStage.getTestList)
                        {
                            Console.WriteLine("ID no. " + test.IDRet + "Type: " + test.typeRet);
                            Console.WriteLine("Status: ");
                            if (test.Completed)
                            {
                                Console.WriteLine("Completed");
                                if (test.Passed)
                                    Console.WriteLine("Passed");
                                else
                                    Console.WriteLine("Failed");
                            }
                            else
                                Console.WriteLine("Not yet carried out");
                            Console.WriteLine();

                        }
                        Console.WriteLine("===========================================");
                        Console.WriteLine();
                        break;
                    case 5:
                        Console.WriteLine("===========================================");
                        Console.WriteLine("Please enter the ID nuber of the test you wish to edit");
                        Console.WriteLine("===========================================");
                        Console.WriteLine();
                        tempTestID = Convert.ToInt32(Console.ReadLine());
                        Test tempTest = tempStage.getTestList.Find(x => x.IDRet.Equals(tempTestID));
                        if (tempTest != null)
                            testMenu(tempList, tempProj, tempSpool, tempStage, tempTest);
                        else
                        {
                            Console.WriteLine("===========================================");
                            Console.WriteLine("This test does not yet exist");
                            Console.WriteLine("Please create the test before accessing the menu");
                            Console.WriteLine("===========================================");
                        }
                        Console.WriteLine();
                        break;
                    default:
                        break;
                }
            } while (stageSwitch != 0);
            spoolMenu(tempList, tempProj, tempSpool);
        }

        static void testMenu(ProjectList tempList, Project tempProj, Spool tempSpool, Stage tempStage, Test tempTest)
        {
            int testSwitch;
            String check;
            int pass;

            do
            {
                Console.WriteLine("===========================================");
                Console.WriteLine("1. Mark test as complete");
                Console.WriteLine("0. Exit to Project menu");
                Console.WriteLine("===========================================");
                Console.WriteLine();
                testSwitch = Convert.ToInt32(Console.ReadLine());
                switch (testSwitch)
                {
                    case 1:
                        Console.WriteLine("===========================================");
                        Console.WriteLine("Are you sure you want to mark this test as complete? y/n");
                        Console.WriteLine("===========================================");
                        Console.WriteLine();
                        check = Console.ReadLine();
                        if (check == "y" || check == "yes")
                        {
                            tempTest.changeComplete = true;
                            Console.WriteLine("===========================================");
                            Console.WriteLine("Has the spool passed the test?");
                            Console.WriteLine("1. Yes");
                            Console.WriteLine("2. No");
                            Console.WriteLine("===========================================");
                            Console.WriteLine();
                            pass = Convert.ToInt32(Console.ReadLine());
                            do
                            {
                                Console.WriteLine("===========================================");
                                Console.WriteLine("Input error, please try again");
                                Console.WriteLine("Has the spool passed the test?");
                                Console.WriteLine("1. Yes");
                                Console.WriteLine("2. No");
                                Console.WriteLine("===========================================");
                                Console.WriteLine();
                                pass = Convert.ToInt32(Console.ReadLine());

                            } while (pass != 1 || pass != 2);
                            Console.WriteLine("===========================================");
                            if (pass == 1)
                            {
                                tempTest.passComplete = true;
                                Console.WriteLine("Test record updated");
                            }
                            if (pass == 2)
                            {
                                Console.WriteLine("Test record updated");
                            }
                            Console.WriteLine("===========================================");
                            Console.WriteLine();
                        }
                        else
                            Console.WriteLine("===========================================");
                            Console.WriteLine("Returning to menu");
                            Console.WriteLine("===========================================");
                        Console.WriteLine();
                        break;
                    default:
                        break;
                }
            } while (testSwitch != 0);
            stageMenu(tempList, tempProj, tempSpool, tempStage);
        }

        static void Main(string[] args)
        {            
            ProjectList newProjList = new ProjectList();
            topMenu(newProjList);
        }
    }
}

