﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace system_prototype
{
    class Weld
    {
        private Char type;
        private int welder_ID;        

        public Weld(Char type, int welder_ID)
        {
            this.type = type;
            this.welder_ID = welder_ID;
        }

        public Char Weld_type
        {
            get { return type; }
        }

        public int WelderID
        {
            get { return welder_ID; }
        }
    }
}
